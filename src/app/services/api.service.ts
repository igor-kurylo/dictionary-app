import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor() {
    }

    /**
     * get words list
     * @returns {any[]}
     */
    getWordsList() {
        let words = [];
        if (localStorage.getItem('app-words')) {
            words = JSON.parse(localStorage.getItem('app-words'));
        } else {
            for (let i = 1; i <= 100; i++) {
                const obg = {};
                obg['en'] = 'word-en-' + i;
                obg['ru'] = 'word-ru-' + i;

                words.push(obg);
            }
        }
        return words;
    }
}
