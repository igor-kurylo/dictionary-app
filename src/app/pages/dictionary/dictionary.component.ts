import {ChangeDetectorRef, Component, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BsModalRef, BsModalService, PageChangedEvent} from 'ngx-bootstrap';
import {ApiService} from '../../services/api.service';

@Component({
    selector: 'app-dictionary',
    templateUrl: './dictionary.component.html',
    styleUrls: ['./dictionary.component.scss']
})
export class DictionaryComponent implements OnInit {

    modalRef: BsModalRef;

    public addWordForm: FormGroup;
    public editWordForm: FormGroup;

    public modelWords = [];
    public paginationViewItem = [];
    public currentPage: number = 1;
    public listForRemove = [];
    public numberItemsOnPage = 10;
    public isAllChecked: boolean = false;
    public checkedAllListPage = [];

    private startItem: number = 0;
    private endItem: number = this.numberItemsOnPage;
    private editWordIndex: number;

    constructor(
        private fb: FormBuilder,
        private modalService: BsModalService,
        private apiService: ApiService
    ) { }

    ngOnInit() {
        this.modelWords = this.apiService.getWordsList();

        this.addWordForm = this.fb.group({
            enWord: ['', Validators.required],
            ruWord: ['', Validators.required]
        });

        this.editWordForm = this.fb.group({
            editEnWord: ['', Validators.required],
            editRuWord: ['', Validators.required]
        });

        this.addCheckStatus(this.modelWords);
        this.paginationViewItem = this.modelWords.slice(0, this.numberItemsOnPage);
    }

    /**
     * change page
     * @param {PageChangedEvent} event
     */
    pageChanged(event: PageChangedEvent): void {
        this.currentPage = event.page;
        this.startItem = (event.page - 1) * event.itemsPerPage;
        this.endItem = event.page * event.itemsPerPage;
        this.paginationViewItem = this.modelWords.slice(this.startItem, this.endItem);

        this.setAllChecked();
    }

    /**
     * add to each word the status of the selection
     * @param array
     */
    addCheckStatus(array) {
        array.forEach(item => {
            item['checked'] = false;
        });
    }

    /**
     * get a current word index
     * @param {number} index
     * @returns {number}
     */
    getCurrentIndex(index: number) {
        return index + (this.currentPage - 1) * this.numberItemsOnPage;
    }

    /**
     * select all items
     * @param e
     */
    selectAll(e) {
        const firstIndex = this.modelWords.indexOf(this.paginationViewItem[0]);

        if (e.target.checked) {
            this.isAllChecked = true;
            this.checkedAllListPage.push(this.currentPage);

            for (let i = 0; i < this.paginationViewItem.length; i++) {
                this.modelWords[firstIndex + i]['checked'] = true;
                this.listForRemove.push(firstIndex + i);
            }
        } else {
            this.isAllChecked = false;

            for (let i = 0; i < this.paginationViewItem.length; i++) {
                this.modelWords[firstIndex + i]['checked'] = false;
            }

            let position = this.listForRemove.indexOf(firstIndex);
            this.listForRemove.splice(position ,this.numberItemsOnPage);

            position = this.checkedAllListPage.indexOf(this.currentPage);
            position >= 0 && this.checkedAllListPage.splice(position, 1);
        }
    }

    /**
     * select one item
     * @param e
     * @param {number} index
     */
    selectItem(e, index: number) {
        const isChecked = e.target.checked
        this.modelWords[index]['checked'] = isChecked;

        if (isChecked) {
            this.listForRemove.push(index);
        } else {
            const position = this.listForRemove.indexOf(index);
            position >= 0 && this.listForRemove.splice(position, 1);
        }
        this.listForRemove.sort((a, b) => {
            return a - b;
        });
    }

    /**
     * set true value for all checked input
     */
    setAllChecked() {
        this.isAllChecked = false;

        this.checkedAllListPage.forEach(item => {
            this.currentPage === item && (this.isAllChecked = true);
        });
    }

    /**
     * add a new word to list
     */
    addWord() {
        this.modelWords.unshift({
            en: this.addWordForm.value.enWord,
            ru: this.addWordForm.value.ruWord,
            checked: false
        });
        localStorage.setItem('app-words', JSON.stringify(this.modelWords));
        this.updateItemView();
        this.addWordForm.reset();
    }

    /**
     * edit word
     */
    editWord() {
        this.modelWords[this.editWordIndex] = {
            en: this.editWordForm.value.editEnWord,
            ru: this.editWordForm.value.editRuWord,
            checked: false
        }
        localStorage.setItem('app-words', JSON.stringify(this.modelWords))
        this.updateItemView();
        this.modalRef.hide();
    }

    /**
     * remove a word from list
     * @param index
     */
    removeWord(index) {
        this.modelWords.splice(index, 1);
        localStorage.setItem('app-words', JSON.stringify(this.modelWords))
        this.updateItemView();
    }

    /**
     * remove few selected words
     */
    removeSelectedWords() {
        for (let i = 0; i < this.listForRemove.length; i++) {
            this.modelWords.splice(this.listForRemove[i] - i, 1);
            this.updateItemView();
        }
        localStorage.setItem('app-words', JSON.stringify(this.modelWords))
        this.listForRemove = [];
        this.checkedAllListPage = [];
        this.isAllChecked = false;
    }

    /**
     * update view word list
     */
    updateItemView() {
        if (this.paginationViewItem.length === 1) {
            this.startItem -= this.numberItemsOnPage;
            this.endItem -= this.numberItemsOnPage;
        }
        this.paginationViewItem = this.modelWords.slice(this.startItem, this.endItem);
    }

    /**
     * open edit word form modal
     * @param {TemplateRef<any>} template
     * @param {string} enWord
     * @param {string} ruWord
     * @param {number} index
     */
    openModal(template: TemplateRef<any>, enWord: string, ruWord: string, index: number) {
        this.modalRef = this.modalService.show(template);
        this.editWordForm.patchValue({
            editEnWord: enWord,
            editRuWord: ruWord
        });
        this.editWordIndex = index;
    }

}
