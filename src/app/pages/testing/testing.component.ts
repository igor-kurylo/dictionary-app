import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';

@Component({
    selector: 'app-testing',
    templateUrl: './testing.component.html',
    styleUrls: ['./testing.component.scss']
})
export class TestingComponent implements OnInit {

    public numberAnswerItem: number = 6;
    public numberWordPairs: number = 20;
    public wordsList = [];
    public wordsListOrigin = [];
    public wordListForTesting = [];
    public position: number = 0;
    public answer = [];
    public isTestingStart: boolean = false;
    public isTestingEnd: boolean = false;
    public correctAnswer: number = 0;
    public currentWord: object;

    constructor(private apiService: ApiService) {
    }

    ngOnInit() {
        this.wordsList = this.apiService.getWordsList();
        this.wordsListOrigin = this.wordsList.slice();

        for (let i = 0; i < this.numberWordPairs; i++) {
            let index = this.randomInteger(0, this.wordsList.length -1),
                item = this.wordsList.splice(index, 1);
            this.wordListForTesting.push(item[0]);
        }
    }

    /**
     * generate random integer
     * @param min
     * @param max
     * @returns {any}
     */
    randomInteger(min, max) {
        let rand = min + Math.random() * (max - min);
        rand = Math.round(rand);
        return rand;
    }

    /**
     * initialize testing step
     */
    initTestingStep() {
        this.answer = [];
        this.currentWord = this.wordListForTesting[this.position];
        let array = this.wordsListOrigin.slice();

        this.answer.push(this.currentWord['ru']);

        for (let i = 1; i < this.numberAnswerItem;) {
            const index = this.randomInteger(0, array.length - 1);

            if (this.currentWord['ru'] !== array[index].ru) {
                const item = array.splice(index, 1);
                this.answer.push(item[0].ru);
                i++;
            }
        }
        this.answer.sort((a, b) => 0.5 - Math.random());
    }

    /**
     * start testing
     */
    startTesting() {
        this.position = 0;
        this.correctAnswer = 0;
        this.isTestingEnd = false;
        this.isTestingStart = true;

        this.initTestingStep();
    }

    /**
     * choose word event and check end testing
     * @param {string} word
     */
    nextWord(word: string) {
        this.position++;
        if (this.position < this.numberWordPairs) {
            this.calculationResult(word);
            this.initTestingStep();
        } else {
            this.endTesting();
        }
    }

    /**
     * calculation a correct answer
     * @param {string} word
     */
    calculationResult(word: string) {
        this.currentWord['ru'] === word && this.correctAnswer++;
    }

    /**
     * end testing and show a result form
     */
    endTesting() {
        this.isTestingStart = false;
        this.isTestingEnd = true;
    }

}
