import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DictionaryComponent} from './pages/dictionary/dictionary.component';
import {TestingComponent} from './pages/testing/testing.component';

const routes: Routes = [
    { path: '', redirectTo: 'dictionary', pathMatch: 'full'},
    { path: 'dictionary', component: DictionaryComponent },
    { path: 'testing', component: TestingComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
