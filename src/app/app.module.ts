import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AppRoutingModule} from './app-routing.module';

import { AppComponent } from './app.component';
import { DictionaryComponent } from './pages/dictionary/dictionary.component';
import { TestingComponent } from './pages/testing/testing.component';
import { HeaderComponent } from './blocks/header/header.component';
import { AddWordsComponent } from './pages/dictionary/add-words/add-words.component';
import { EditWordsComponent } from './pages/dictionary/edit-words/edit-words.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ModalModule, PaginationModule} from 'ngx-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    DictionaryComponent,
    TestingComponent,
    HeaderComponent,
    AddWordsComponent,
    EditWordsComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
